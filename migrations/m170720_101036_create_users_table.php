<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m170720_101036_create_users_table extends Migration
{
	/**
	 * @inheritdoc
	 */
	public function up()
	{
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			// http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}

		$this->createTable('users', [
			'id' => $this->primaryKey(),
			'username' => $this->string()->notNull(),
			'password' => $this->string()->notNull(),
			'auth_key' => $this->string()->notNull(),
			'created_at' => $this->integer()->notNull(),
			'updated_at' => $this->integer()->notNull()
		], $tableOptions);

		$password = Yii::$app->security->generatePasswordHash('admin');
		$password2 = Yii::$app->security->generatePasswordHash('demo');
		$auth_key = Yii::$app->security->generateRandomString();
		$auth_key2 = Yii::$app->security->generateRandomString();

		$this->insert('users', [
			'username' 		=> 'admin',
			'password' 		=> $password,
			'auth_key' 		=> $auth_key,
			'created_at' 	=> time(),
			'updated_at' 	=> time(),
		]);

		$this->insert('users', [
			'username' 		=> 'demo',
			'password' 		=> $password2,
			'auth_key' 		=> $auth_key2,
			'created_at' 	=> time(),
			'updated_at' 	=> time(),
		]);

	}

	/**
	 * @inheritdoc
	 */
	public function down()
	{
		$this->dropTable('users');
	}
}
